import co.poynt.postman.PostmanCollectionRunner;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestPostman {


    @Test
    public void testRunPostman() throws Exception {
        PostmanCollectionRunner cr = new PostmanCollectionRunner();

        boolean isSuccessful = cr.runCollection(
                "src/test/resources/runCollection/PostmanRunnerRegression.postman_collection.json",
                "src/test/resources/runCollection/PostmanRunnerRegression.postman_environment.json",
                "runCollection", true).isSuccessful();

        Assert.assertTrue(isSuccessful);
    }
}
